package basic;

public class CashPayment implements PaymentMethod {
    @Override
    public void pay(int bani) {
        System.out.println("Paid " + bani + " in cash.");
    }
}
