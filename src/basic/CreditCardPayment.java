package basic;

public class CreditCardPayment implements PaymentMethod {
    @Override
    public void pay(int bani) {
        System.out.println("Paid " + bani + " using my credit card.");
    }
}
