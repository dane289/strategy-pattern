package basic;

public class DebitCardPayment implements PaymentMethod {
    @Override
    public void pay(int bani) {
        System.out.println("Paid " + bani + " using my debit card.");
    }
}
