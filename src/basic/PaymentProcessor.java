package basic;

public class PaymentProcessor {
    private PaymentMethod paymentMethod;

    public PaymentProcessor() {
    }

    public PaymentProcessor(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void pay(int amount) {
        paymentMethod.pay(amount);
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public static void main(String[] args) {
        PaymentProcessor paymentProcessor = new PaymentProcessor(new CashPayment());
        paymentProcessor.pay(500);
        paymentProcessor.setPaymentMethod(new DebitCardPayment());
        paymentProcessor.pay(800);
        paymentProcessor.setPaymentMethod(new CreditCardPayment());
        paymentProcessor.pay(5000);
        paymentProcessor.setPaymentMethod(new BitCoinPayment());
        paymentProcessor.pay(5999999);

    }
}
