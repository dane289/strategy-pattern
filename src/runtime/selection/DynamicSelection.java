package runtime.selection;

import basic.*;

import java.util.Random;

public class DynamicSelection {
    public static void main(String[] args) {
        PaymentProcessor paymentProcessor = new PaymentProcessor();
        int amount = new Random().nextInt(1000000);

        if(amount < 100) {
            paymentProcessor.setPaymentMethod(new CashPayment());
        } else if (amount < 500) {
            paymentProcessor.setPaymentMethod(new DebitCardPayment());
        } else if (amount < 1000){
            paymentProcessor.setPaymentMethod(new CreditCardPayment());
        } else {
            paymentProcessor.setPaymentMethod(new BitCoinPayment());
        }

        paymentProcessor.pay(amount);
    }
}
