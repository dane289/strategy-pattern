package runtime.selection.factory;

import basic.*;

public class PaymentMethodFactory {

    public static PaymentMethod getPaymentMethod(int amount) {
        if(amount < 100) {
            return new CashPayment();
        } else if (amount < 500) {
            return new DebitCardPayment();
        } else if (amount < 1000){
            return new CreditCardPayment();
        } else {
            return new BitCoinPayment();
        }
    }
}
