package runtime.selection.factory;

import basic.PaymentMethod;

public class PaymentMethodFactoryCurrentCentury {

    public static PaymentMethod crediCardPayment(int amount) {
        return (sum) -> System.out.println("Paid " + sum + " using my credit card.");
    }

    public static PaymentMethod debitCardPayment(int amount) {
        return (sum) -> System.out.println("Paid " + sum + " using my debit card.");
    }

    public static PaymentMethod cashPayment(int amount) {
        return (sum) -> System.out.println("Paid " + sum + " in cash.");
    }

    public static PaymentMethod bitcoinPayment(int amount) {
        return (sum) -> System.out.println("Alex paind " + sum + " in bitcoin.");
    }

    public static PaymentMethod getPaymentMethiod(int amount) {
        if(amount < 100) {
            return cashPayment(amount);
        } else if (amount < 500) {
            return debitCardPayment(amount);
        } else if (amount < 1000){
            return crediCardPayment(amount);
        } else {
            return bitcoinPayment(amount);
        }
    }
}
