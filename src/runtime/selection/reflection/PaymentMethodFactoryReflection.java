package runtime.selection.reflection;

import basic.PaymentMethod;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

public class PaymentMethodFactoryReflection {

    public static Optional<PaymentMethod> getPaymentMethod(String className) {
        try {
            Class<?> aClass = Class.forName(className);
            return Optional.of((PaymentMethod) aClass.getDeclaredConstructor().newInstance());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            return Optional.empty();
        }
    }
}
