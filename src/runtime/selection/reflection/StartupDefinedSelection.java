package runtime.selection.reflection;

import basic.PaymentMethod;
import basic.PaymentProcessor;

import java.util.Optional;

public class StartupDefinedSelection {

    public static void main(String[] args) {
        PaymentProcessor paymentProcessor = new PaymentProcessor();
        Optional<PaymentMethod> paymentMethod = PaymentMethodFactoryReflection.getPaymentMethod("basic.CashPayment");
        paymentMethod.ifPresent(paymentMethod1 -> {paymentProcessor.setPaymentMethod(paymentMethod1);
            paymentProcessor.pay(800);
        });
        paymentMethod.orElseThrow( () -> new IllegalArgumentException("No payment method"));
    }
}
