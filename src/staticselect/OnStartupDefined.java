package staticselect;

import basic.*;

public class OnStartupDefined {
    public static void main(String[] args) {
        PaymentMethod paymentMethod;
        switch (args[0]) {
            case "0" :
                paymentMethod = new DebitCardPayment();
                break;
            case "1" :
                paymentMethod = new CreditCardPayment();
                break;
            default:
                paymentMethod = new CashPayment();
                break;
        }

        new PaymentProcessor(paymentMethod).pay(5000);
    }
}
