package staticselect.propertyfile;

import basic.PaymentMethod;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.Properties;

public class PaymentMethodPropertyFactory {
    public static Optional<PaymentMethod> getPaymentMethod() {
        try {
            Optional<String> paymentMethodClassName = getPaymentMethodClassName();
            if (paymentMethodClassName.isPresent()) {
                Class<?> aClass = Class.forName(paymentMethodClassName.get());
                return Optional.of((PaymentMethod) aClass.getDeclaredConstructor().newInstance());
            } else {
                return Optional.empty();
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            return Optional.empty();
        }
    }

    private static Optional<String> getPaymentMethodClassName() {
        try {
            Properties properties = new Properties();
            InputStream propertyFile = PaymentMethodPropertyFactory.class.getClassLoader().getResourceAsStream("staticselect//propertyfile//startup.properties");
            properties.load(propertyFile);
            return Optional.ofNullable(properties.getProperty("payment.classname"));
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}
