package staticselect.propertyfile;

import basic.PaymentProcessor;

public class PropertyFileDefinedStartup {
    public static void main(String[] args) {
        PaymentMethodPropertyFactory.getPaymentMethod().ifPresent(method ->  {
            PaymentProcessor paymentProcessor = new PaymentProcessor(method);
            paymentProcessor.pay(90909);
        });
    }
}
